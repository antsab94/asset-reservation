CREATE SCHEMA IF NOT EXISTS DEV;

CREATE TABLE employee(
    id              bigserial primary key,
    name            varchar (120) not null,
    seniority       varchar (120) not null
);

CREATE TABLE asset(
    id              bigserial primary key,
    type            varchar (120) not null,
    optlock_version integer NOT NULL DEFAULT(0)
);

CREATE TABLE reservation(
    employee_id     bigserial REFERENCES employee,
    asset_id        bigserial REFERENCES asset,
    submit_date     timestamp,
    start_date      timestamp,
    end_date        timestamp,

    PRIMARY KEY(employee_id, asset_id)
);

