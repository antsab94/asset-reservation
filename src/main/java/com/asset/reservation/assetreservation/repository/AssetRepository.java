package com.asset.reservation.assetreservation.repository;

import com.asset.reservation.assetreservation.model.Asset;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AssetRepository extends JpaRepository<Asset, Long> {
}
