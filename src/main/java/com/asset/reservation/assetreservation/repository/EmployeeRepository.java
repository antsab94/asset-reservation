package com.asset.reservation.assetreservation.repository;

import com.asset.reservation.assetreservation.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
}
