package com.asset.reservation.assetreservation.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Builder
public class ReservationId implements Serializable {

    @Column(name = "employee_id")
    private Long employeeId;

    @Column(name = "asset_id")
    private Long assetId;
}
