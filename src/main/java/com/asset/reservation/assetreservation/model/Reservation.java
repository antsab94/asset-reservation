package com.asset.reservation.assetreservation.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Builder
@Table(name = "reservation")
public class Reservation {

    @Getter
    @Setter
    @EmbeddedId
    private ReservationId id;

    @Getter
    @Setter
    @Column(name = "submit_date")
    private LocalDate submitDate;

    @Getter
    @Setter
    @Column(name = "start_date")
    private LocalDate startDate;

    @Getter
    @Setter
    @Column(name = "end_date")
    private LocalDate endDate;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("employeeId")
    private Employee employee;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("asset_id")
    private Asset asset;

    @Override
    public String toString() {
        return "Reservation{" +
                "id=" + id +
                ", submitDate=" + submitDate +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", employee=" + employee +
                ", asset=" + asset +
                '}';
    }
}
