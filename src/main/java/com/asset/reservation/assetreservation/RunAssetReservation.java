package com.asset.reservation.assetreservation;

import com.asset.reservation.assetreservation.model.Asset;
import com.asset.reservation.assetreservation.model.Employee;
import com.asset.reservation.assetreservation.model.Reservation;
import com.asset.reservation.assetreservation.model.ReservationId;
import com.asset.reservation.assetreservation.repository.AssetRepository;
import com.asset.reservation.assetreservation.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component
@RequiredArgsConstructor
@Transactional
public class RunAssetReservation implements CommandLineRunner {


    private final EmployeeRepository employeeRepository;
    private final AssetRepository assetRepository;

    private final Scanner scanner = new Scanner(System.in);
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

    private void insertEmployees() {
        System.out.println("Inserting employees in DB.");

        List<Employee> employees = IntStream.range(1, 5)
                .mapToObj(i -> Employee.builder().name("name" + i).seniority("senior").build()).collect(Collectors.toList());
        employeeRepository.saveAll(employees);

        System.out.println("DONE!");
    }

    private void insertAssets() {
        System.out.println("Inserting assets in DB.");

        List<Asset> assets = IntStream.range(1, 5)
                .mapToObj(i->Asset.builder().type("laptop").build()).collect(Collectors.toList());

        assets.addAll(
                IntStream.range(1, 5)
                        .mapToObj(i->Asset.builder().type("charger").build()).collect(Collectors.toList())
        );
        assets.addAll(
                IntStream.range(1, 5)
                        .mapToObj(i->Asset.builder().type("screen").build()).collect(Collectors.toList())
        );
        assetRepository.saveAll(assets);

        System.out.println("DONE!");
    }

    private void createReservation(Employee employee) {
        System.out.println("");
        System.out.println("Insert asset id");
        Long assetId = Long.valueOf(scanner.nextLine());
        Asset asset = assetRepository.getOne(assetId);

        System.out.println("");
        System.out.println("Insert start date. Format is dd-MM-yyyy");
        LocalDate startDate = LocalDate.parse(scanner.nextLine(), formatter);
        System.out.println("");
        System.out.println("Insert end date. Format is dd-MM-yyyy");
        LocalDate endDate = LocalDate.parse(scanner.nextLine(), formatter);

        List<Reservation> reservations = asset.getReservations();
        reservations = reservations != null? reservations : Collections.EMPTY_LIST;

        Optional<Reservation> overlappingReservation = reservations
                .stream()
                .filter(reservation ->
                        startDate.isBefore(reservation.getEndDate()) && reservation.getStartDate().isBefore(endDate))
                .findFirst();

        if (overlappingReservation.isPresent()) {
            System.out.println("");
            System.out.println("Reservation date is overlapping with another reservation");
            System.out.println(overlappingReservation.get());
            System.out.println("");
        } else {
            ReservationId reservationId = ReservationId.builder().employeeId(employee.getId()).assetId(assetId).build();
            Reservation newReservation = Reservation.builder()
                    .id(reservationId)
                    .employee(employee)
                    .asset(asset)
                    .submitDate(LocalDate.now())
                    .startDate(startDate)
                    .endDate(endDate)
                    .build();

            employee.getReservations().add(newReservation);
            asset.getReservations().add(newReservation);

            System.out.println("");
            System.out.println("Successfully reserved!");
        }
    }

    private void displayAvailableAssets() {
        System.out.println("");
        System.out.println(assetRepository.findAll());
        System.out.println("");
    }

    private void returnAsset(Employee employee) {
        System.out.println("");
        System.out.println("Employee's reservation list " +employee.getReservations());
        System.out.println("");
        System.out.println("Insert id of asset you want to return");

        Long assetId = Long.valueOf(scanner.nextLine());
        ReservationId reservationId = ReservationId.builder()
                .assetId(assetId)
                .employeeId(employee.getId())
                .build();

        /* This will save us from an extra db trip  */
        for(Iterator<Reservation> iterator = employee.getReservations().iterator();iterator.hasNext();) {
            Reservation reservation = iterator.next();

            if (reservation.getId().equals(reservationId)) {
                iterator.remove();
                reservation.getAsset().getReservations().remove(reservation);

                reservation.setEmployee(null);
                reservation.setAsset(null);
            }
        }
    }

    @Override
    public void run(String... args) {
        insertEmployees();
        insertAssets();

        loop: while(true) {
            System.out.println("");
            System.out.println(employeeRepository.findAll());
            System.out.println("");
            System.out.println("Insert employee's id that you want to impersonate");

            Long id = Long.valueOf(scanner.nextLine());
            Employee employee = employeeRepository.getOne(id);

            System.out.println("\n");
            System.out.println("Select what action you want to perform");
            System.out.println("1- display the available assets; 2- reserve an asset; 3- return an asset; 4- exit");

            int actionType = Integer.parseInt(scanner.nextLine());

            switch (actionType) {
                case 1:
                    displayAvailableAssets();
                    break;
                case 2:
                    createReservation(employee);
                    break;
                case 3:
                    returnAsset(employee);
                    break;
                default:
                    break loop;
            }
        }

        System.out.println("\n");
        System.out.println("Done!");
    }
}
