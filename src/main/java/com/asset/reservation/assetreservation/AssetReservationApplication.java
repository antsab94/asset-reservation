package com.asset.reservation.assetreservation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AssetReservationApplication {

	public static void main(String[] args) {
		SpringApplication.run(AssetReservationApplication.class, args);
	}

}
